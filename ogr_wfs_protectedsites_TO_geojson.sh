#!/bin/bash
# Autor: Stanislav Motyčka
# Opis: Skript pre vytvorenie GoeJSON dátovej sady z WFS chránených území, ako podkladu pre JOSM a následný upload do OpenStreetMap
# # # https://www.openstreetmap.org/user/StanoM/diary/391084
# Použitie: "ogr_wfs_protectedsites_TO_geojson.sh 2"
# # # výsledkom skriptu je súbor typu ".geojson", ktorý sa použije ako vstup do JOSM
# v editore [JOSM](https://josm.openstreetmap.de/) je potrebné mať nainštalovaný zásuvný modul "geojson" 

### KONTROLA povinného číselného parametra
if [[ "$1" =~ ^[0-9]+$ ]]; then
  CATEGORY="$1" 
else
  echo -e "nebol zadany povinny parameter \"ČÍSLO KATEGÓRIE chráneného územia\"" 
  exit 0
fi

# zistenie počtu prvkov:
# http://www.sopsr.sk/geoserver/sopsr/ows?service=WFS&version=1.1.0&request=GetFeature&typeName=sopsr:IG_PROTECTEDSITES_ZONE&CQL_FILTER=CATEGORY=${CATEGORY}&srsName=EPSG:4326&resultType=hits
# http://www.sopsr.sk/geoserver/sopsr/ows?service=WFS&version=1.1.0&request=GetFeature&typeName=sopsr:IG_vPROTECTEDSITES_VCHU&CQL_FILTER=CATEGORY%20IN(8,9,10)&srsName=EPSG:4326&resultType=hits

### PREMENNE >>> ###

OUTPUT=/tmp/category_${CATEGORY}.geojson
ROK=$(date +%Y)
ROK="${ROK} " 

### PREMENNE <<< ###

# podľa hodnoty ${CATEGORY} sa nastavia OSM tagy, s ktorými sa chránené územia nahrajú do OSM, a to v zmysle existujúcich OSM tagových hodnôt: 
# # # https://wiki.openstreetmap.org/wiki/Tag:boundary%3Dprotected_area
case ${CATEGORY} in
    # NPR
     1)    
          OSM_boundary="protected_area" 
          OSM_protect_class="1" 
          ;;
    # PR
     2)    
          OSM_boundary="protected_area" 
          OSM_protect_class="1" 
          ;;
    # NPP
     3)    
          OSM_boundary="protected_area" 
          OSM_protect_class="3" 
          ;;
    # PP
     4)    
          OSM_boundary="protected_area" 
          OSM_protect_class="3" 
          ;;
    # CHA
     5)    
          OSM_boundary="protected_area" 
          OSM_protect_class="4" 
          ;;
    # OP PR
     6)    
          OSM_boundary="protected_area" 
          OSM_protect_class="4" 
          ;;
    # OP CHA
     7)    
          OSM_boundary="protected_area" 
          OSM_protect_class="4" 
          ;;
    # NP
     8)    
          OSM_boundary="protected_area" 
          OSM_protection_title="National Park" 
          OSM_protect_class="2" 
          ;;
    # OP NP
     9)      
          OSM_boundary="protected_area" 
          OSM_protection_title="Protected Landscape Area" 
          OSM_protect_class="5" 
          ;;
    # CHKO
     10)      
          OSM_boundary="protected_area" 
          OSM_protection_title="Protected Landscape Area" 
          OSM_protect_class="5" 
          ;;
    # SKCHVU
     20)      
          OSM_boundary="protected_area" 
          OSM_protection_title="Special Protected Areas" 
          OSM_protect_class="97" 
          ;;
    # SKUEV
     21)      
          OSM_boundary="protected_area" 
          OSM_protection_title="Sites of Community Importance" 
          OSM_protect_class="97" 
          ;;
     *)
          OSM_boundary="protected_area" 
          OSM_protect_class="99" 
          ;;
esac

rm -f ${OUTPUT}
#ogr2ogr -overwrite -f GeoJSON ${OUTPUT} "WFS:http://www.sopsr.sk/geoserver/sopsr/ows?service=WFS&version=1.1.0&request=GetFeature&typeName=sopsr:IG_PROTECTEDSITES_ZONE&CQL_FILTER=CATEGORY=${CATEGORY}&srsName=EPSG:4326" -nln "wfs_IG_PROTECTEDSITES"  -nlt MULTIPOLYGON -sql "SELECT 'boundary' AS type, '${OSM_boundary}' AS boundary, 'sk_sopsr' AS import_ref, '${OSM_protect_class}' AS protect_class, SITETITLE_SK as name, SITETITLE_SK as \"name:sk\", SITETITLE_EN as \"name:en\", DEGREE as \"sopsr:zone\", CONCAT('(C)','${ROK}','Štátna ochrana prírody SR Banská Bystrica http://www.sopsr.sk') AS source, CONCAT('http://www.sopsr.sk') AS \"contact:website\", 'https://www.facebook.com/sopsr.sk' AS \"contact:facebook\", '' As description, SHAPE AS geometry FROM IG_PROTECTEDSITES_ZONE" 
nazov_uzemia=
#nazov_uzemia=$(echo " AND SITETITLE_SK LIKE 'NP Muránska planina'" |uni2ascii -s -a J)
#echo "${nazov_uzemia}" 
ogr2ogr \
    -overwrite \
    -f GeoJSON ${OUTPUT} \
    "WFS:http://www.sopsr.sk/geoserver/sopsr/ows?service=WFS&version=2.0.0&request=GetFeature&typeName=sopsr:IG_PROTECTEDSITES_ZONE&CQL_FILTER=CATEGORY=${CATEGORY}${nazov_uzemia}&srsName=EPSG:5514" \
    -nln "wfs_IG_PROTECTEDSITES" \
    -nlt MULTIPOLYGON \
    -sql "SELECT 'boundary' AS type, '${OSM_boundary}' AS \"boundary\", 'sk_sopsr' AS \"import_ref\", '45' AS \"sopsr:id\", '${OSM_protect_class}' AS \"protect_class\", SITETITLE_SK as \"name\", SITETITLE_SK as \"name:sk\", SITETITLE_EN as \"name:en\", DEGREE as \"sopsr:zone\", CONCAT('(C)','${ROK}','Štátna ochrana prírody SR Banská Bystrica http://www.sopsr.sk') AS \"source\", CONCAT('http://www.sopsr.sk') AS \"contact:website\", 'https://www.facebook.com/sopsr.sk' AS \"contact:facebook\", SITETITLE_SK AS \"description\", SHAPE AS \"geometry\" FROM IG_PROTECTEDSITES_ZONE" \
    -s_srs "+proj=krovak +lat_0=49.5 +lon_0=42.5 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +pm=ferro +units=m +no_defs +towgs84=485.021,169.465,483.839,7.786342,4.397554,4.102655,0" \
    -t_srs "EPSG:4326" 
#    -s_srs "+proj=krovak +lat_0=49.5 +lon_0=42.5 +alpha=30.28813972222222 +k=0.9999 +x_0=-0 +y_0=-0 +ellps=bessel +pm=ferro +to_meter=-1 +no_defs +towgs84=511.9,92.0,437.3,6.305,2.823,5.944,12.2" \

#ogr2ogr -overwrite -f GeoJSON ${OUTPUT} "WFS:http://www.sopsr.sk/geoserver/sopsr/ows?service=WFS&version=2.0.0&request=GetFeature&typeName=sopsr:IG_PROTECTEDSITES_ZONE&srsName=EPSG:4326" -nln "wfs_IG_PROTECTEDSITES"  -nlt MULTIPOLYGON -sql "SELECT 'boundary' AS type, '${OSM_boundary}' AS boundary, 'sk_sopsr' AS import_ref, '${OSM_protect_class}' AS protect_class, SITETITLE_SK as name, SITETITLE_SK as \"name:sk\", SITETITLE_EN as \"name:en\", DEGREE as \"sopsr:zone\", CONCAT('(C)','${ROK}','Štátna ochrana prírody SR Banská Bystrica http://www.sopsr.sk') AS source, CONCAT('http://www.sopsr.sk') AS \"contact:website\", 'https://www.facebook.com/sopsr.sk' AS \"contact:facebook\", '' As description, SHAPE AS geometry FROM IG_PROTECTEDSITES_ZONE WHERE CATEGORY=${CATEGORY} AND SITETITLE_SK LIKE 'NP Murán%'"